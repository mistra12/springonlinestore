package com.codeflext.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * untuk menandai starting point aplikasi spring boot ditandai dengan annotation @SpringBootAplication
 * argument scanBasePackages={"com.codeflext.springboot"} memberi tahu spring bahwa base packagenya adalah com.codeflext.springboot
 */
@SpringBootApplication(scanBasePackages={"com.codeflext.springboot"})
public class SpringbootRestOnlineStore {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootRestOnlineStore.class, args);
    }
}
